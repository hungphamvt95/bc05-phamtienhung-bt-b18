var numArr = [];
var slSoDuongGloBal = 0;
var slSoAmGloBal = 0;
function themSo() {
  var slSoDuong = 0;
  var slSoAm = 0;
  var tongSoDuong = 0;

  var num = document.getElementById("txt-so-n").value * 1;
  numArr.push(num);
  document.getElementById("result1").innerHTML = ` Mảng: ${numArr} `;

  var soNN = numArr[0];
  var soChanCuoiCung;
  var slSoChan = 0;
  var soDuongArr = [];

  for (var index = 0; index < numArr.length; index++) {
    var currentNum = numArr[index];
    if (currentNum > 0) {
      tongSoDuong = tongSoDuong + currentNum;
      slSoDuong = slSoDuong + 1;
      slSoDuongGloBal = slSoDuong;
      soDuongArr.push(currentNum);
    } else if (currentNum < 0) {
      slSoAm = slSoAm + 1;
      slSoAmGloBal = slSoAm;
    }
    if (soNN > currentNum) {
      soNN = currentNum;
    }
    if (currentNum % 2 == 0) {
      soChanCuoiCung = currentNum;
      slSoChan = slSoChan + 1;
    }
  }

  if (slSoChan == 0) {
    soChanCuoiCung = -1;
  }

  var soDuongNN = soDuongArr[0];
  for (var i = 0; i < soDuongArr.length; i++) {
    var soDuongCurr = soDuongArr[i];
    if (soDuongNN > soDuongCurr) {
      soDuongNN = soDuongCurr;
    }
  }

  document.getElementById("result2").innerHTML = ` 
  Tổng số dương trong mảng là: ${tongSoDuong} `;
  document.getElementById("result3").innerHTML = ` 
  Số các số dương trong mảng là: ${slSoDuong} `;
  document.getElementById("result4").innerHTML = ` 
  Số nhỏ nhất trong mảng là: ${soNN} `;
  document.getElementById("result5").innerHTML = ` 
  Số dương nhỏ nhất trong mảng là: ${soDuongNN} `;
  document.getElementById("result6").innerHTML = ` 
  Số chẵn cuối cùng trong mảng là: ${soChanCuoiCung} `;
  var num = (document.getElementById("txt-so-n").value = "");
}

function doiViTri() {
  var viTri1 = document.getElementById("txt-vtri1").value * 1;
  var viTri2 = document.getElementById("txt-vtri2").value * 1;
  var numTrungGian = numArr[viTri1];
  numArr[viTri1] = numArr[viTri2];
  numArr[viTri2] = numTrungGian;
  document.getElementById("result7").innerHTML = ` 
  Mảng sau khi đổi: ${numArr} `;
}

var numTrungGian1;
function sapXepMang() {
  for (var i = 0; i < numArr.length; i++) {
    for (var j = 0; j < numArr.length - i - 1; j++) {
      if (numArr[j] > numArr[j + 1]) {
        numTrungGian1 = numArr[j];
        numArr[j] = numArr[j + 1];
        numArr[j + 1] = numTrungGian1;
      }
    }
  }
  document.getElementById("result8").innerHTML = ` 
  Mảng sau khi sắp xếp: ${numArr} `;
}

function timSoNguyenTo1() {
  var soNguyenToDauTien;
  var daCoSoNT = 0;
  for (var index = 0; index < numArr.length; index++) {
    var soLanChiaHet = 0;
    var soKt = numArr[index];

    if (soKt > 1) {
      for (var i = 2; i < soKt; i++) {
        if (soKt % i == 0) {
          soLanChiaHet = soLanChiaHet + 1;
        }
      }
      if (soLanChiaHet == 0) {
        soNguyenToDauTien = soKt;
        daCoSoNT = 1;
        break;
      } else {
        daCoSoNT = 0;
      }
    }
  }
  if (daCoSoNT == 1) {
    console.log("Số nguyên tố đầu tiên là: ", soNguyenToDauTien);
    document.getElementById("result9").innerHTML = ` 
    Kết quả: ${soNguyenToDauTien} `;
  } else {
    document.getElementById("result9").innerHTML = ` Kết quả: -1 `;
  }
}

var realNumArr = [];
function themSoThuc() {
  var slSoNguyen = 0;
  var realNum = document.getElementById("txt-so-thuc").value * 1;
  realNumArr.push(realNum);
  document.getElementById("result10").innerHTML = ` 
  Mảng số thực: ${realNumArr} `;
  for (let index = 0; index < realNumArr.length; index++) {
    if (Number.isInteger(realNumArr[index])) {
      slSoNguyen = slSoNguyen + 1;
    }
  }
  document.getElementById("result11").innerHTML = ` 
  Số các số nguyên trong mảng là: ${slSoNguyen} `;
  var realNum = (document.getElementById("txt-so-thuc").value = "");
}

function soSanh() {
  if (slSoDuongGloBal > slSoAmGloBal) {
    document.getElementById("result12").innerHTML = ` 
    Số lượng số dương > Số lượng số âm `;
  } else if (slSoDuongGloBal < slSoAmGloBal) {
    document.getElementById("result12").innerHTML = ` 
    Số lượng số dương < Số lượng số âm `;
  } else {
    document.getElementById("result12").innerHTML = ` 
    Số lượng số dương = Số lượng số âm `;
  }
}
